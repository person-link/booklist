# BookList

Book List for Person Link

## 開発環境構築

#### 1. gitクローン

`$ git clone git@bitbucket.org:person-link/booklist.git`

#### 2. claspインストール

`$ npm i @google/clasp -g`

#### 3. claspログイン

`$ clasp login`

#### 4. コードの取得

`$ cd bookList`

`$ clasp pull`

## 参考
[GAS のGoogle謹製CLIツール clasp - Qiita](https://qiita.com/HeRo/items/4e65dcc82783b2766c03)

