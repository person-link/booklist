var sheet = SpreadsheetApp.getActiveSheet();

// スプレッドシートを直接編集またはGlide経由で編集が行われた時に呼び出される
function onChange(e) {
  Logger.log(JSON.stringify(e));
  var activeRow = sheet.getActiveCell().getRow();
  var activeColumn = sheet.getActiveCell().getColumn();

  switch (activeColumn) {
    case 1:
      const isbn = sheet.getRange(activeRow, 1).getValue();
      if (!isbn) removeBook();
      else addBook(isbn, activeRow);
      break;
    case 9:
      const lendUser = sheet.getRange(activeRow, 9).getValue();
      if (!lendUser) returnBook();
      else lendBook();
      break;
  }
}

//GETリクエスト、ここではカメラスキャンした時に呼び出される
function doGet(e) { 
  const isbn = e.parameter.isbn;
  // const isbn = 9784863541863;
  Logger.log(isbn);

  const response = UrlFetchApp.fetch('https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn + '&country=JP');
  const data = JSON.parse(response.getContentText());
  Logger.log(data);

  var htmlTemplate = HtmlService.createTemplateFromFile("src/result");
  var result;

  if (!data.items) {
    htmlTemplate.text = '書籍情報が見つかりません';
    result = false;

  } else {
    htmlTemplate.isbn = isbn;
    result = true;

    const bookInfo = data.items[0].volumeInfo;
    if(bookInfo.imageLinks && bookInfo.imageLinks.thumbnail) htmlTemplate.img = bookInfo.imageLinks.thumbnail;
    if (bookInfo.title) htmlTemplate.title = bookInfo.title;
    if(bookInfo.authors) htmlTemplate.authors = bookInfo.authors.join();
  }

  htmlTemplate.result = result;

  var html = htmlTemplate.evaluate()
  html
    .setTitle('Book List for Person Link')
    .addMetaTag('viewport', 'width=device-width, initial-scale=1');
  return html;

  //return HtmlService.createHtmlOutput(JSON.stringify(e));
}

//POSTリクエスト、ここではカメラスキャン経由で書籍登録が行われた時に呼び出される
function doPost(e) {
  Logger.log(e);

  var isbn = e.parameter.isbn;
  var owner = e.parameter.owner;
  var place = e.parameter.place;
  var borrower = e.parameter.borrower;

  var insertRow = sheet.getLastRow()+1;
  addBook(isbn, insertRow);
  sheet.getRange(insertRow,7).setValue(owner);
  sheet.getRange(insertRow,8).setValue(place);
  sheet.getRange(insertRow,9).setValue(borrower);

  var htmlTemplate = HtmlService.createTemplateFromFile("src/registered");
  var html = htmlTemplate.evaluate()
  html
    .setTitle('Book List for Person Link')
    .addMetaTag('viewport', 'width=device-width, initial-scale=1');
  return html;
}